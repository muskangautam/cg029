#include <stdio.h>
int main()
{
    int i,j,m,n;
    printf("Enter the rows and columns of both the matrix\n");
    scanf("%d%d",&m,&n);
    int a[m][n],b[m][n],sum[m][n],sub[m][n];
    printf("Enter the first matrix\n");
    for(i=0;i<m;i++)
    {
        for(j=0;j<n;j++)
            scanf("%d",&a[i][j]);
    }
    printf("Enter the second matrix\n");
    for(i=0;i<m;i++)
    {
        for(j=0;j<n;j++)
            scanf("%d",&b[i][j]);
    }
    for(i=0;i<m;i++)
    {
        for(j=0;j<n;j++)
        {
            sum[i][j]=a[i][j]+b[i][j];
            sub[i][j]=a[i][j]-b[i][j];
        }
    }
    printf("Sum matrix\n");
    for(i=0;i<m;i++)
    {
        for(j=0;j<n;j++)
            printf(" %d ",sum[i][j]);
        printf("\n");
    }
    printf("Difference matrix\n");
    for(i=0;i<m;i++)
    {
        printf("\n");
        for(j=0;j<n;j++)
            printf(" %d ",sub[i][j]);
    }
    return 0;

}