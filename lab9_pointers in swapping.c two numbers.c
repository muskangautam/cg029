#include<stdio.h>
void swapnum (int *var1,int *var2)
{
int tempnum;
tempnum=*var1;
*var1=*var2;
*var2=tempnum;
}
int main()
{
int num1=18,num2=10;
printf("before swapping");
printf("\num1 value is %d",num1);
printf("\num2 value is %d",num2);
swapnum(&num1,&num2);
printf("after swapping");
printf("\num1 value is %d",num1);
printf("\num2 value is %d",num2);
return 0;
}